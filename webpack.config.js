var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        'whatwg-fetch',
        "./app/index.js"
    ],
    output: {
        path: path.resolve(__dirname, "public"),
        publicPath: "/",
        filename: "scripts/main.js",
        sourceMapFilename: 'scripts/main.js.map'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0','react-hmre'],
                    plugins: ['transform-decorators-legacy'],
                },
                exclude: /node_modules/
            }
        ]
    },
    devServer: {
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),
    ]
};
