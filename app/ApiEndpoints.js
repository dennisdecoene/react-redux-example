/**
 * Created by dennis on 08/08/16.
 */
export const server = "http://127.0.0.1:8002";

export const assignments = server + "/assignments/";
export const userLogin = server + "/login/";