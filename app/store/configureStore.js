/**
 * Created by dennis on 12/08/16.
 */

import {applyMiddleware, createStore, compose, combineReducers} from "redux"
import {reduxReactRouter, routerStateReducer, ReduxRouter} from 'redux-router';
import {createHistory} from 'history';
import thunk from "redux-thunk"
import createLogger from 'redux-logger';

import DevTools from "../DevTools"
import rootReducer from "../reducers"

export default function configureStore(initialState) {
    let createStoreWithMiddleware;

    const logger = createLogger();

    const middleware = applyMiddleware(thunk, logger);

    createStoreWithMiddleware = compose(
        middleware,
        DevTools.instrument()
    );

    const store = createStoreWithMiddleware(createStore)(rootReducer, initialState);

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index');
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;

}

