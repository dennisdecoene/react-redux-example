/**
 * Created by dennis on 12/08/16.
 */

import axios from "axios";
import {stopSubmit} from 'redux-form';

import * as Endpoints from "../ApiEndpoints";
import * as actions from "../ActionTypes";

function fetchAssignmentDataRequest() {
    return {type: actions.FETCHING_ASSIGNMENTDATA}
}

function assignmentDataRecieved(data) {
    return {type: actions.FETCH_ASSIGNMENTDATA_FULFILLED, payload: data}
}

function assignmentDataRejected(err) {
    return {type: actions.FETCH_ASSIGNMENTDATA_REJECTED, payload: err}
}

function submitNewAssignmentRequest(){
    return {type: actions.SUBMITTING_NEW_ASSIGNMENT}
}

function newAssignmentSubmitted(data){
    return {type: actions.SUBMITTING_NEW_ASSIGNMENT_FULFILLED, payload: data}
}

function newAssignmentSubmitRejected(err) {
    return {type: actions.SUBMITTING_NEW_ASSIGNMENT_REJECTED, payload: err}
}

export function fetchAssignments(username, password) {
    return function (dispatch) {
        dispatch(fetchAssignmentDataRequest());
        axios.get(Endpoints.assignments, {auth: {username, password}})
            .then((response) => {
                //console.log("response",response);
                dispatch(assignmentDataRecieved(response.data))
            })
            .catch((err) => {
                dispatch(assignmentDataRejected(err))
            })
    };
}

export function submitNewAssignment(values, dispatch) {
    const {customer, reference, contract, provider} = values;
    axios.post(Endpoints.assignments, {customer, reference, contract, provider})
            .then((response)=> {
                dispatch(newAssignmentSubmitted(response.data));
                dispatch(stopSubmit('assignmentForm'));
            })
            .catch((err)=> {
                dispatch(stopSubmit('assignmentForm', err));
            });
}