/**
 * Created by dennis on 12/08/16.
 */
import axios from "axios";
import * as Endpoints from "../ApiEndpoints";
import * as actions from "../ActionTypes";

export function logoutUser() {
    return function (dispatch) {
        axios.defaults.headers.common['Authorization'] = '';
        dispatch({type: actions.LOGOUT_USER})
    };
}

export function fetchUser(username, password) {
    return function (dispatch) {
        dispatch({type: actions.FETCHING_USER});
        return axios.post(Endpoints.userLogin, {username, password})
            .then((response) => {
                axios.defaults.headers.common['Authorization'] = response.config.headers.Authorization;
                dispatch({type: actions.FETCH_USER_FULFILLED, payload: {data: response.data, username, password}});
            })
            .catch((err) => {
                dispatch({type: actions.FETCH_USER_REJECTED, payload: err})
            })
    };
}
