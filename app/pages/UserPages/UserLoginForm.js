/**
 * Created by dennis on 03/08/16.
 */
import React from 'react';
import {Button} from 'react-bootstrap';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {fetchUser} from "../../actions/userActions";

class UserLoginForm extends React.Component {

    static contextTypes = {
        store: React.PropTypes.object,
        router: React.PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            user_id: '',
            password: '',
        };
        this.unsubscribe = ()=>{};
    }

    onChangeUser(e) {
        this.setState({user_id: e.target.value});
    }

    onChangePassword(e) {
        this.setState({password: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.fetchUser(this.state.user_id,this.state.password)
            .then(()=>{this.context.router.push("/")});
    }

    componentWillMount() {
    }

    componentWillUnmount(){
    }

    render() {
        return (

            <div className="container">

                <div id="loginbox" className="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">

                    <div className="row">
                        <div className="col-xs-12">
                            <img src="/img/intelligentlogo.jpg" className="center-block" style={{width: 150}}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <div className="panel-title text-center">Babel Login</div>
                                </div>

                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className={this.props.user.statusText? "alert alert-danger":"hidden"}>
                                                {this.props.user.statusText}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form className="form-horizontal">

                                                <div className="input-group">
                                                    <span className="input-group-addon"><span
                                                        className="glyphicon glyphicon-user"> </span></span>
                                                    <input id="user" type="text" className="form-control"
                                                           name="user"
                                                           placeholder="User" onChange={this.onChangeUser.bind(this)}
                                                           value={this.state.user}/>
                                                </div>

                                                <div className="input-group">
                                                    <span className="input-group-addon"><span
                                                        className="glyphicon glyphicon-lock"/></span>
                                                    <input id="password" type="password" className="form-control"
                                                           name="password"
                                                           placeholder="Password" onChange={this.onChangePassword.bind(this)}
                                                           value={this.state.password}/>
                                                </div>

                                                <div className="form-group">
                                                    <div className="col-sm-12 controls">
                                                        <Button onClick={this.handleSubmit.bind(this)}
                                                                className="btn btn-primary pull-right"
                                                                disabled={this.props.user.isFetching}>
                                                            <span className="glyphicon glyphicon-log-in"> </span>
                                                            &nbsp;&nbsp;Log in
                                                        </Button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state, ownProps) {
  return { user: state.user }
}

export default connect(mapStateToProps,{fetchUser})(UserLoginForm);