/**
 * Created by dennis on 09/08/16.
 */
import React from 'react';
import {connect} from "react-redux";

import {logoutUser} from "../../actions/userActions";

@connect((store) => {
    return {
        user: store.user,
    };
})
export default class UserLogout extends React.Component {
    componentDidMount() {
        //do Logout
        this.props.dispatch(logoutUser())
    }

    render() {
        return <p>You are now logged out</p>
    }
};