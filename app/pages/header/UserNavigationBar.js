/**
 * Created by dennis on 14/07/16.
 */
import React from 'react';
import { connect } from "react-redux";
import {Link} from 'react-router';

class UserNavigationBar extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
    }

    render() {
        const {user} = this.props;
        var button = '';
        if (user.signed_in) {
            button = <li><Link to="/logout">Logout</Link></li>
        } else {
            button = <li><Link to="/login">Login</Link></li>
        }
        return (
            <div>
                <ul className="nav navbar-nav navbar-right">
                    {button}
                    <li><Link to="/profile">Profile</Link></li>
                </ul>
            </div>
        );
    }
}
;

export default connect((store) => {
    return {
        user: store.user,
    }
})(UserNavigationBar)