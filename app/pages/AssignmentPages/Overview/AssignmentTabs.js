import React from 'react';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import Badge from 'react-bootstrap/lib/Badge';
import {browserHistory} from 'react-router';
import {connect} from "react-redux";
import ReactSpinner from 'react-spinjs';

import AssignmentList from './AssignmentList';
import {fetchAssignments} from '../../../actions/assignmentActions';


@connect((store) => {
    return {
        user: store.user,
        assignments: store.assignments,
    };
})
export default class AssignmentTabs extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: 1
        };
    }

    handleSelect(key) {
        this.setState({key});
    }

    componentDidMount(){
        this.props.dispatch(fetchAssignments(this.props.user.email, this.props.user.password));
    };

    render() {
        var {assigned, assigned_count, unassigned, unassigned_count, archived, archived_count, errorText}=this.props.assignments;

        return (

            <div>
                {this.props.assignments.isFetching ? <ReactSpinner/> :
                    <div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className={errorText ? "alert alert-danger" : "hidden"}>
                                    {errorText}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12">
                                <Tabs activeKey={this.state.key} onSelect={this.handleSelect.bind(this)}
                                      id="controlled-tab-example">
                                    <Tab eventKey={1}
                                         title={<span>Assigned <Badge
                                             className="alert-warning">{assigned_count}</Badge></span>}>
                                        <AssignmentList key={1} assignment_data={assigned}/>
                                    </Tab>
                                    <Tab eventKey={2} title={<span>Unassigned <Badge
                                        className="alert-success">{unassigned_count}</Badge></span>}>
                                        <AssignmentList key={2} assignment_data={unassigned}/>
                                    </Tab>
                                    <Tab eventKey={3} title={<span>Archived <Badge
                                        className="alert-info">{archived_count}</Badge></span>}>
                                        <AssignmentList key={3} assignment_data={archived}/>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </div>
                }
            </div>

        );
    }
}