/**
 * Created by dennis on 14/07/16.
 */
import React from 'react';
import AssignmentListItem from './AssignmentListItem';

export default class AssignmentList extends React.Component {
    constructor(props) {
        super(props);

        this.renderAssignmentListItem = this.renderAssignmentListItem.bind(this);
    };

    renderAssignmentListItem(assignment) {
            return <AssignmentListItem key={assignment.id} assignment={assignment}/>
    };

    render() {
        const mappedData = this.props.assignment_data.map(this.renderAssignmentListItem);
        return (
            <div className="panel">
                <div className="panel-body">
                    <div className="row">
                        <div className="col-xs-12">
                            <table className="col-xs-12">
                                <thead>
                                <tr>
                                    <th>State</th>
                                    <th>Last Change</th>
                                    <th>Reference</th>
                                    <th>Provider</th>
                                    <th>Contract</th>
                                    <th>Customer</th>
                                    <th>Assignee</th>
                                </tr>
                                </thead>
                                <tbody>
                                {mappedData}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}
