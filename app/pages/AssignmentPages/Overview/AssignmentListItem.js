/**
 * Created by dennis on 14/07/16.
 */
import React from 'react';

export default class AssignmentListItem extends React.Component {

    render() {
        return (
            <tr>
                <td>{this.props.assignment.state}</td>
                <td>{this.props.assignment.updated_at}</td>
                <td>{this.props.assignment.reference}</td>
                <td>{this.props.assignment.provider}</td>
                <td>{this.props.assignment.contract}</td>
                <td>{this.props.assignment.customer}</td>
                <td>{this.props.assignment.assignee_id}</td>
            </tr>
        );
    }
}