/**
 * Created by dennis on 17/08/16.
 */
import React, {Component, PropTypes} from "react";
import {startSubmit,reduxForm} from 'redux-form';

import {submitNewAssignment} from "../../../actions/assignmentActions";

export const fields = ['customer', 'reference', 'contract', 'provider',];
export const formName = 'assignmentForm';

const asyncSubmit = (values, dispatch)=> {
    //dispatch(startSubmit(formName));
    dispatch(submitNewAssignment(values,dispatch))
}

class AssignmentForm extends Component {
    render() {
        console.log(this.props);
        const {fields:{customer, reference, contract, provider}, handleSubmit, submitting} = this.props;
        return (
            <div>
                <form className="form-horizontal" onSubmit={handleSubmit(asyncSubmit)}>
                    <div className="modal-body"><h2>New Assignment</h2>
                        <fieldset className="form-horizontal">
                            <div className="form-group">
                                <label className="col-md-2 control-label">Customer number</label>
                                <div className="col-md-10">
                                    <input type="text" placeholder="12345" className="form-control" {...customer}/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-2 control-label">Reference</label>
                                <div className="col-md-10">
                                    <input type="text" placeholder="" className="form-control" {...reference}/>
                                </div>
                            </div>
                            <div className="form-group"><label className="col-md-2 control-label">Contract</label>
                                <div className="col-md-10">
                                    <input type="text" placeholder="12345" className="form-control" {...contract}/>
                                </div>
                            </div>
                            <div className="form-group"><label className="col-md-2 control-label">Provider</label>
                                <div className="col-md-10">
                                    <select className="form-control " {...provider}>
                                        <option label="" value="number:-1"></option>
                                        <option label="Combell" value="number:0">Combell</option>
                                        <option label="Combell Holding" value="number:103">Combell Holding</option>
                                        <option label="Easyhost" value="number:12">Easyhost</option>
                                        <option label="Netbulk" value="number:13">Netbulk</option>
                                        <option label="be.sentia" value="number:15">be.sentia</option>
                                        <option label="Unitt.nl" value="number:14">Unitt.nl</option>
                                        <option label="Register" value="number:102">Register</option>
                                        <option label="StoneIS" value="number:20">StoneIS</option>
                                    </select>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-sm-12 controls">
                                    <button className="btn btn-primary pull-right" type="submit" disabled={submitting}>
                                        <span className="glyphicon glyphicon-plus-sign"> </span>
                                        &nbsp;&nbsp;Create Assignment
                                    </button>
                                </div>
                            </div>
                        </fieldset>

                    </div>
                </form>
            </div>
        );
    }
};

AssignmentForm.propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired
};

export default reduxForm(
    {
        form: formName,
        fields
    },
    state => ({
        user: state.user // mapStateToProps
    }),
    // mapDispatchToProps
)(AssignmentForm);

//export default connect(mapStateToProps, {submitNewAssignment})(AssignmentForm);