import React from "react";
import {connect} from "react-redux";
import AssignmentTabs from './Overview/AssignmentTabs'
import UserLoginForm from "../UserPages/UserLoginForm"

class AssignmentOverview extends React.Component {

    static contextTypes = {
        store: React.PropTypes.object,
        router: React.PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>{this.props.user.signed_in === true ?
                <AssignmentTabs/> : <UserLoginForm/>}
            </div>
        );
    }
};

export default connect((state)=> {
    return {
        user: state.user
    }
})(AssignmentOverview);