import React from "react";
import {connect} from "react-redux";
import AssignmentFormDetail from './Form/AssignmentFormDetail'
import UserLoginForm from "../UserPages/UserLoginForm"
import submitNewAssignment from "../../actions/assignmentActions";

class AssignmentForm extends React.Component {

    static contextTypes = {
        store: React.PropTypes.object,
        router: React.PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>{this.props.user.signed_in === true ?
                <AssignmentFormDetail/> : <UserLoginForm/>}
            </div>
        );
    }
};

export default connect((state)=> {
    return {
        user: state.user
    }
})(AssignmentForm);