import React from 'react';
import { Link } from 'react-router';
import UserNav from './header/UserNavigationBar';

export default class IndexPage extends React.Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-12 navbar navbar-default navbar-fixed-top" role="nav">
                        <div className="container">
                            <div className="navbar-header">
                                <a className="navbar-brand" href="#">Babel</a>
                            </div>
                            <div className="collapse navbar-collapse">
                                <ul className="nav navbar-nav">
                                    <li><Link to="/">Assignments</Link></li>
                                    <li><Link to="/assignmentform">Create Assignment</Link></li>
                                </ul>
                                <UserNav/>
                            </div>
                        </div>
                    </div>
                </div>

                {this.props.children}
            </div>
        );
    }
}
