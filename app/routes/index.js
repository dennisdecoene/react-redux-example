/**
 * Created by dennis on 17/08/16.
 */
import React from "react";
import {Route, IndexRoute} from "react-router";
import IndexPage from "../pages/IndexPage";
import AssignmentOverview from "../pages/AssignmentPages/AssignmentOverview";
import AssignmentForm from "../pages/AssignmentPages/AssignmentForm";
import UserLoginForm from "../pages/UserPages/UserLoginForm";
import UserProfile from "../pages/UserPages/UserProfile";
import UserLogout from "../pages/UserPages/UserLogout";

export default (
    <Route path="/" component={IndexPage}>
        <IndexRoute component={AssignmentOverview}/>
        <Route path="login" component={UserLoginForm}/>
        <Route path="logout" component={UserLogout}/>
        <Route path="profile" component={UserProfile}/>
        <Route path="assignmentform" component={AssignmentForm}/>
    </Route>
);