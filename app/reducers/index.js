/**
 * Created by dennis on 12/08/16.
 */
import { combineReducers } from "redux";
import {routerStateReducer} from 'redux-router';
import {reducer as formReducer} from 'redux-form';

import userReducer from "./userReducer";
import assignmentReducer from "./assignmentReducer";

export default combineReducers({
    user: userReducer,
    assignments: assignmentReducer,
    form: formReducer,
});