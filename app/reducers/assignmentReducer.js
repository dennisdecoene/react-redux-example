/**
 * Created by dennis on 12/08/16.
 */
import * as actions from "../ActionTypes";

function setAssignmentData(data) {

    const assigned = data.filter(function (assignment) {
        return assignment.state === "assigned"
    });
    const unassigned = data.filter(function (assignment) {
        return assignment.state === "unassigned"
    });
    const archived = data.filter(function (assignment) {
        return assignment.state === "archived"
    });
    return ({
        assigned,
        unassigned,
        archived,
        assigned_count: assigned.length,
        unassigned_count: unassigned.length,
        archived_count: archived.length,
        errorText: null,
        isFetching: false,
        isSubmitting: false,
    });
}

const initialState = {
    assigned: [],
    unassigned: [],
    archived: [],
    assigned_count: 0,
    unassigned_count: 0,
    archived_count: 0,
    errorText: null,
    isFetching: false,
    isSubmitting: false,
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.FETCH_ASSIGNMENTDATA_FULFILLED: {
            return Object.assign({}, state, setAssignmentData(action.payload));//{...state, assignments: action.payload};
        }
        case actions.FETCH_ASSIGNMENTDATA_REJECTED: {
            return Object.assign({}, state, initialState, {errorText: action.payload.response.data.reason});
        }
        case actions.FETCHING_ASSIGNMENTDATA: {
            return Object.assign({}, state, initialState, {isFetching: true});
        }
        case actions.SUBMITTING_NEW_ASSIGNMENT: {
            return Object.assign({}, state, {isSubmitting: true});
        }
    }
    return state;
};
