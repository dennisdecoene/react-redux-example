/**
 * Created by dennis on 12/08/16.
 */
import * as actions from "../ActionTypes";

function setUser(data) {
    var signon_timestamp = Date.now();
    return ({
        id: user.id,
        email: data.username,
        password: data.password,
        signed_in: true,
        profile: data.data,
        signon_timestamp: signon_timestamp,
        statusText: 'You have been successfully logged in.',
        isFetching: false,
    });
}

const empty_user = {
    id: 0,
    email: "none",
    password: "",
    signed_in: false,
    profile: null,
    signon_timestamp: null,
    statusText: null,
    isFetching: false,
};

export default function reducer(state = empty_user, action) {
    switch (action.type) {
        case actions.FETCH_USER_FULFILLED: {
            return Object.assign({}, state, setUser(action.payload)); //{...state, user: setUser(action.payload)};
        }
        case actions.FETCH_USER_REJECTED: {
            return Object.assign({}, state, empty_user, {statusText: action.payload.response.data.message});
        }
        case actions.LOGOUT_USER: {
            return Object.assign({}, state, empty_user);//{...state, user: empty_user};
        }
        case actions.FETCHING_USER:{
            return Object.assign({}, state, empty_user, {isFetching:true});
        }
    }
    return state;
};