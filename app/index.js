import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Router} from "react-router";
import {hashHistory} from "react-router";

import routes from "./routes";
import configureStore from "./store/configureStore";
import DevTools from "./DevTools";

const app = document.getElementById("app");
const store = configureStore(window.__INITIAL_STATE__);

const root = (
    <Provider store={store}>
        <div>
            <Router history={hashHistory}>
                {routes}
            </Router>
            <DevTools/>
        </div>
    </Provider>
);

ReactDOM.render(root, app);
