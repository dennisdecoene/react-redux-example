## Install

`npm install`

## Build

`webpack`

When you run `webpack`, it will build in `public/scripts/main.js`. 

## Test

`npm test`

## Dev Start

`npm start`

